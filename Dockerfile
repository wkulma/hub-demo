FROM node

RUN npm install -g bower \
    npm install -g polymer-cli
    
ADD . /app

WORKDIR /app

RUN npm install

COPY . /usr/src/app

EXPOSE 8080

CMD [ "npm", "start" ]